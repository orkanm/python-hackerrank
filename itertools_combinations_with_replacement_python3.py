# Enter your code here. Read input from STDIN. Print output to STDOUT

from itertools import combinations_with_replacement  #  Import itertools,

A, B = input().split()  #   Seperate inputs.
ordered = sorted(A) #   Sort the input list.

combinations = list(combinations_with_replacement(ordered,int(B))) #    Prepare the combinations.

for i in range(0, len(combinations)):
    print(''.join(combinations[i])) #   print combinations one by one and as string.

