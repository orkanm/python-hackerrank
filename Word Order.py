from collections import defaultdict

n = int(input())
word_count = defaultdict(int)

for _ in range(n):
    word = input()
    word_count[word] += 1

reducedWord = list(word_count.keys())
occurances = list(word_count.values())

print(len(reducedWord))
print(*occurances)