x, k = input().split()  # Split input data.
p = input() # Get polynom.
res = int(eval(p.replace('x',x, -1)))   # Replace the polynom variables with input 'x'.
                                        # Evaluate the value of function.

if (int(k) == res): # If result of evaluation is equal to k 
    print(True)  
else:
    print(False)