from collections import namedtuple # Import NamedTuple.
  
n = int(input()) # Get number of entries.
summ=0 # Sum variable for the MARKS.
data = namedtuple('data', input().split()) # Get data types for the tuple.

for i in range(n): summ+=int(data(*(input().split())).MARKS) # Get MARk values in the tuple.
    
print (summ/n) # Print average of the MARKS.
