import re # Import regex.

n = int(input()) # Get desired number for regex check.

for i in range(n): # Loop for each input check.
    
    try: 
        if(re.compile(input())): # Check regex valid or not. 
            print(True) # If result of the check is valid, print True.

    except re.error: # If result of the check is not valid, print false.
        print(False) 
