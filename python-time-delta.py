#!/bin/python3

import math
import os
import random
import re
import sys
from datetime import datetime  # check https://docs.python.org/3/library/datetime.html#module-datetime for further information.

# Complete the time_delta function below.
def time_delta(t1, t2):
    t1 = datetime.strptime(t1, "%a %d %b %Y %H:%M:%S %z") # Parse t1.
    t2 = datetime.strptime(t2, "%a %d %b %Y %H:%M:%S %z") # Parse t2.
   
    t1 = int((t1-t2).total_seconds()) # ind delta of t1 and t2.

    if (t1 < 0):  # Check sign of delta.
        t1 = t1 * (-1)
    return t1

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(input())

    for t_itr in range(t):
        t1 = input()
        t2 = input()
        
        delta = time_delta(t1, t2)
        
        fptr.write(str(delta) + '\n')

    fptr.close()