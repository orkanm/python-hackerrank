# Enter your code here. Read input from STDIN. Print output to STDOUT
N = int(input())
A = set(map(int, input().split()))

for _ in range(int(input())):
    command, *value = input().split()  # split command and args.
    R = set(map(int, input().split()))
    command = getattr(A, command)  # merge set and command (like: s.pop() ).
    command(R)   # set args for command.

print(sum(A))