n = int(input())
s = set(map(int, input().split()))
N = int(input())

for i in range(N):
    command, *value = input().split()  # split command and args.
    command = getattr(s, command)  # merge set and command (like: s.pop()  ) .
    command(*[int(x) for x in value])   # set args for command.

print(sum(s))