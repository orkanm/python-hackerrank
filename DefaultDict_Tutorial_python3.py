# Enter your code here. Read input from STDIN. Print output to STDOUT
from collections import defaultdict
d1 = defaultdict(list)
d2 = defaultdict(list)

n, m = map(int, input().split())

for i in range(0,n):
    d1[i].append(input())
for i in range(0,m):
    d2[i].append(input())
    
for i in range(0,len(d2.items())):
    if(d2[i] in d1.values()):
        keys = [k+1 for k, v in d1.items() if v == d2[i]]
        print(*keys, sep = ' ')
    else:
        keys=[-1]
        print(*keys, sep = ' ')
