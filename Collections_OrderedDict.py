# Enter your code here. Read input from STDIN. Print output to STDOUT
from collections import OrderedDict

od = OrderedDict() # Ordered dictionary.

number = int(input()) # Number of items.

for i in range(0,number): # Calculation loop for the dictionary items.
    item = input().rsplit(' ', 1) # Seperate last word of the input.
    if(item[0] in od.keys()): # Put items to the dictionary.
        od[item[0]] = int(od[item[0]]) + int(item[1]) # If item is already in the dictionary sum the last enty of item.
    else:
        od[item[0]] = int(item[1]) # Else item is not in the dictionary, add item to the dictionary
    
for key, value in od.items(): # Print the ordered dict key and value.
    print(key, value)
    
