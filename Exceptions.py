n = int(input()) # Get 'n' input for division.

for i in range (n): # Loop for read and calculate 'n' input.
    a, b = input().split() # Read and seperate division elements.
 
    try: # Try to divide elements.
        print(int(a)//int(b)) 
    except ZeroDivisionError as e: # Exception for the ZeroDivisionError.
        print("Error Code:",e)
    except ValueError as e: # Exception for the ValueError.
        print("Error Code:",e)
