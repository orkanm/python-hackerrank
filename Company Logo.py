#!/bin/python3

if __name__ == '__main__':
    s = input()
    out = {x : s.count(x) for x in set(s)}
    out = dict(sorted(sorted(out.items(), key=lambda x: x[0]),))
    out = dict(sorted(out.items(), key=lambda x: x[1], reverse=True))
    out= dict(list(out.items())[0: 3])

for k, v in out.items():
    print(k,v)